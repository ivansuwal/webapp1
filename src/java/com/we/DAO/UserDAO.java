
package com.we.DAO;

import com.we.common.DataBaseConnection;
import com.we.models.user;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserDAO {
    public void insertUser(String u) throws SQLException{
        try{
            DataBaseConnection con=new DataBaseConnection();
        
        Connection conn=con.getConnection();
        String sql="Insert into user (name) values('"+u+"')";
        Statement st=conn.createStatement();
        st.execute(sql);
        conn.close();
        }
        catch(Exception e){}
    }
    
    public ArrayList<user> viewUser(){
        ArrayList<user> list=new ArrayList();
        try{
            DataBaseConnection con=new DataBaseConnection();
            Connection conn=con.getConnection();
            String sql="Select *from user";
            Statement st=conn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                user u=new user();
                u.setId(rs.getInt("id"));
                u.setName(rs.getString("name"));
                list.add(u);
            }
            conn.close();
            }
        catch(Exception e){}
        return list;
    }
    
    public void deleteUser(user u){
        try{
            Connection con=DataBaseConnection.getConnection();
            String sql="Delete from user where id=?";
            PreparedStatement st = con.prepareStatement(sql);
            st.setInt(1,u.getId());
            st.executeUpdate();
            con.close();
            }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
