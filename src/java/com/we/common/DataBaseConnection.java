package com.we.common;
import java.sql.Connection;
import java.sql.DriverManager;
public class DataBaseConnection {
    public static Connection getConnection(){
        try{
        Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/user_db?useUnicode=yes&characterEncoding=UTF-8", "root", "");
         return con;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
